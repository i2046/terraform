terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }

}

provider "aws" {
   profile = "default"
   region = "us-east-2"
}

resource "aws_instance" "name" {
    ami = "ami-064ff912f78e3e561"
    instance_type = "t2.micro"
    key_name = "ohio_srujan"

    tags = {
        Name = "terraform"
    }
    user_data = <<-EOF
                #!/bin/bash
                sudo yum update -y
                sudo yum install httpd -y
                sudo systemctl start httpd
                EOF
    
}

# resource "aws_key_pair" "deploy" {
#   key_name   = "ohio_srujan"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnnCd6XTTWcv/PhFtkyoGlh7Q3Vbv1mjSFFNT6rNOf1uqerUhJI1U9FqLa/Pb4ChkIvLXufkstjmDnx/OobEigQEOMmw6PVmp7vx/AP1I8FyCXZIPROhj9yzFfTgMrbg9avSF/UIlUQG9iDl+QevNU+lrIPXiJG0Z/qU1fEjhs6e32Z9+UQRVg7mgYl3fxTcv6NxCVKhzW4llOZDKEW1BTZYy018zyDD/uWgZvR4BOgVI0TWzARKeq7kpBVBPPw5P7+ZxPMmFWQ/If9nHGQ1zAFjxc8ZxM8uISdpf0yawZyBaOY3xVRsPoi/VA7UAtfGyeJyeAbD3+U/BsqTjqaKLv"
# }


  


resource "aws_vpc" "firstvpc" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"
    
    tags = {
      Name = "production"
    }
}

resource "aws_subnet" "subnet-1" {
    vpc_id = aws_vpc.firstvpc.id
    cidr_block = "10.0.0.0/24"
    tags = {
      Name = "pubsubnet"
    }
  
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.firstvpc.id

  tags = {
    Name = "main"
  }
}

resource "aws_eip" "ip" {
    vpc = true
  
}

resource "aws_nat_gateway" "ngw1" {
    allocation_id = aws_eip.ip.id
    subnet_id = aws_subnet.subnet-1.id
  
}

resource "aws_security_group" "allow_webserver" {
  name        = "allow_webserver"
  description = "Allow web inbound traffic"
  
  ingress {
    description      = "ssh from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  ingress {
    description      = "http from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
    

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  tags = {
    Name = "allow_tls"
  }
  }

    


  
